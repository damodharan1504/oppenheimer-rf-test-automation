The Oppenheimer Project - Test Automation

The test automation framework is implemented using Robot Framework. The below are the test cases/scenarios covered
as part of this test automation. The test cases coveres the given userstories. The framework covers both API & UI test automation.

About the framework

Robot Framework is a generic open source automation framework. It can be used for test automation and robotic process automation (RPA).
Ref: https://robotframework.org/#getting-started

Framework Setup - Getting Started
1. Robot Framework is implemented with Python, so you need to have Python installed.
On Windows machines, make sure to add Python to PATH during installation.

For Installing python, go this link and download python 3 version and install in the machine and set the path to system variable
https://www.python.org/downloads/

Installing Robot Framework with pip is simple:

pip install robotframework

To check that the installation was succesful, run

robot --version

Here, we are planned to do both API & UI automation, so we need to install the main libraries. Here the list of libraries
we need to install and the comments listed below. Please refer and install them to run the code.

SeleniumLibrary
pip install --upgrade robotframework-seleniumlibrary

REST
pip install --upgrade RESTinstance

JSON Libaray
pip install robotframework-jsonlibrary

Others are built-in libraries. And there is util.py file is created for implement keywords in python and there is resource file 
for robot framework keywords.


Test Case 01:
Clerk inserts single person to the database - This is an API test adds single record to database using api and validate the response.


Test Case 02:
Clerk inserts multipls persons to the database
- Clerk adds multiple user as a dataset(json file - in test data folder)

Test Case 03:
Clerk uploads persons record of csv file from the UI
- Clerk adds users using csv file (in test data folder)

Test Case 04:
Bookkeeper gets the list of each person tax relief
- Retrieves data using API, validating the natid $$$ characters from 5th characters
- Calculate the relief tax

Test Case 05:
Governor dispenses the tax relief
- Governor dispense the amount using the "Dispense Now" button
- Button color is verified
- Button text is verified
- Navigation to confirmation page is verified



