*** Settings ***
Library    SeleniumLibrary
Library    REST    http://localhost:8080/
Library    String
Library    OperatingSystem
Library    JSONLibrary
Library    util.py
Library    DateTime
Library    Collections
Resource    keywords.resource

Test Setup    Set Screenshot Directory    ${screenshot_dir}



*** Variables ***
${base_url}    http://localhost:8080/
${browser}     chrome
${test_data_dir}    ${CURDIR}/test_data
${output_dir}    ${CURDIR}/output
${screenshot_dir}    ${output_dir}/screenshots


${dispense_btn}    //a[@href='dispense']



*** Test Cases ***

Clerk inserts single person to the database
    POST        /calculator/insert        ${test_data_dir}/single_person.json
    Integer     response status           202        #expected code for post request is 201 but api returns 202
    [Teardown]    Output    response body    ${output_dir}/API/res_single_person.json


Clerk inserts multipls persons to the database
    POST        /calculator/insertMultiple        ${test_data_dir}/multiple_persons.json
    Integer     response status           202        #expected code for post request is 201 but api returns 202
    [Teardown]    Output    response body    ${output_dir}/API/res_multiple_persons.json


Clerk uploads persons record of csv file from the UI
    Open Browser    ${base_url}    ${browser}
    Maximize Browser Window

    #Get csv file records count
    ${file}=    Get File    ${CURDIR}/test_data/insert_persons.csv
    ${number_of_lines}=    Get Line Count    ${file}
    Log    ${number_of_lines}
    ${number_of_records}=    Evaluate    ${number_of_lines}-1    #skipping the header row
    Log    ${number_of_records}

    #Table Person count before csv upload
    ${wch_tax_relief_cnt}=    Get Element Count    //tbody/tr


    #Upload csv records
    Choose File    //input[@type='file']    ${CURDIR}/test_data/insert_persons.csv
    Click Button    //button[.='Refresh Tax Relief Table']

    #Table Person count after csv upload
    ${wch_tax_relief_newcnt}=    Get Element Count    //tbody/tr

    #Comparing the table records - to verify successful csv upload
    Should Be Equal As Numbers    ${wch_tax_relief_cnt}    ${wch_tax_relief_newcnt}

Bookkeeper gets the list of each person tax relief
    ${response}=    GET        /calculator/taxRelief
    Integer     response status           200

    # ${json_object}=    Set Variable    ${response}
    @{natids}=	Get Value From Json	    ${response}	    $..natid
    @{relief}=	Get Value From Json	    ${response}	    $..relief

    FOR  ${natid}  IN  @{natids}

        ${natid_len}=    Get Length    ${natid}
        IF    ${natid_len} > 4    #Check only if natid length greater than 4 characters
            ${natid_masked_actual}=    Get Substring       ${natid}    4    ${natid_len}
            log to console  natid_masked: ${natid_masked_actual}

            ${natid_masked_len}=    Get Length    ${natid_masked_actual}

            #Generating expected masked string to match with actual
            ${natid_masked_exp}=    Generate Random String    ${natid_masked_len}    $
            Should Be Equal As Strings    ${natid_masked_exp}    ${natid_masked_actual}
        END     
    END
    [Teardown]    Output    response body    ${output_dir}/API/tax_relief.json


Tax Releif
    # Get csv file records and set in global variable
    ${records}=    Get File    ${CURDIR}/test_data/insert_persons.csv
    Log    ${records}

    ${number_of_lines}=    Get Line Count    ${records}
    ${int_record}    Set Variable    1
    
    @{records_list}=    Create List
    FOR  ${int_record}  IN RANGE  ${number_of_lines}
        IF  ${int_record} > 0
            ${record}    Get Line    ${records}    ${int_record}
            Log    ${record}
            Append To List    ${records_list}    ${record}
        END
    END

    Set Global Variable    ${records_list}
    Log    ${records_list}

    FOR  ${record}  IN  @{records_list}
        Log    ${record}
        @{record}    Split String    ${record}    ,
        ${natid}=    Set Variable    ${record[0]}
        ${name}=    Set Variable    ${record[1]}
        ${gender}=    Set Variable    ${record[2]}
        ${salary}=    Set Variable    ${record[3]}
        ${birthday}=    Set Variable    ${record[4]}
        ${tax}=    Set Variable    ${record[5]}

        ${tax_relief}    calculate_tax_relief    ${gender}    ${salary}    ${birthday}    ${tax}
        Log    ${tax_relief}
    END
    
    
Governor dispenses the tax relief
    Open Browser    ${base_url}    ${browser}
    Maximize Browser Window
    Element Text Should Be    ${dispense_btn}    Dispense Now    ,    False
    # ${bg color}    Call Method    ${dispense_btn}    value_of_css_property    background-color

    ${elem_dispense}=    Get WebElement    ${dispense_btn}
    ${bg_color}=    Call Method     ${elem_dispense}    value_of_css_property    background-color

    ${red}    Set Variable    rgba(220, 53, 69, 1)
    Run Keyword If    '${red}' == '${bg_color}'    Log    Dispense Now button background color is Red
     ...    ELSE    Fail    Dispense Now button background color is not Red

    Capture Page Screenshot    TaxReliefCalculationPage.png
    Capture Element Screenshot    ${elem_dispense}    DispenseNowButton.png

    #Dispense the cash
    Click Button    ${elem_dispense}
    ${redirect_page_title}    Get Title

    Run Keyword If    '${redirect_page_title}' == 'Dispense!!'    Log    Redirected to cash dispensed page
     ...    ELSE    Fail    Redirected to cash dispensed page failed

    Element Text Should Be    //div[@class='display-4 font-weight-bold']    Cash dispensed    ,    False
    Capture Page Screenshot    CashDispensed.png



                 

    
    

