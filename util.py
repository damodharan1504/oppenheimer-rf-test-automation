from datetime import datetime

def calculate_age(dob_string):
    born = datetime.strptime(dob_string, '%d%m%Y')
    today = datetime.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def calculate_tax_relief(salary, tax_paid, variable, gender_bonus):
    tax_relief = round(((float(salary) - float(tax_paid)) * float(variable)) + float(gender_bonus), 3)
    if tax_relief > 0.00 and tax_relief < 50.00:
        tax_relief = 50.00
    return tax_relief